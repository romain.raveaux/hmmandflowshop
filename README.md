# Hidden Markov Model (HMM) for sequence prediction with an application to the flowshop problem F2||SumCj


# Goals : 
1. A presentation of HMM
2. An application to the flowshop problem

# Author: Romain Raveaux (romain.raveaux@univ-tours.fr)

![FlowShop](http://romain.raveaux.free.fr/document/flowshoppb.PNG "FlowShopPB")
![FlowShop](http://romain.raveaux.free.fr/document/seqwindow.PNG "FlowShopPB")

# Problem definition : 
$\mathcal{D}= \{x_i,y_i\}_{i=1}^N$ with $N$ the number of pair samples.

$x_i \in \mathbb{R}^{M \times 4}$ is a sequence of $M$ jobs. A job is defined by four values : processing times on machine 1 and 2 and completion times on machine 1 and 2.

$y_i \in \{0,1\}^{M \times 1}$ is a sequence representing a window (values equals to 1) where the sequence $x_i$ is to be re-optimized to get a better scheduling.

We want to find a function $f: \mathbb{R}^{M \times 4} \to \{0,1\}^{M \times 1}$.

We want to predict where the sequence $x_i$ should be reoptimized to get a better scheduling.
